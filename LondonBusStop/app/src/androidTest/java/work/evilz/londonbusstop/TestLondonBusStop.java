package work.evilz.londonbusstop;

import android.view.View;

import com.google.android.gms.common.SignInButton;
import com.paypal.android.sdk.payments.PaymentActivity;

import work.evilz.londonbusstop.activities.Activity_Login;
import work.evilz.londonbusstop.activities.Activity_London_Bus_Stops;

/**
 * test Activity_London_Bus_Stops
 * @author ZeNan Gao
 * @version 09/01/2015
 * @since 1.0
 */
public class TestLondonBusStop extends AbstractSystemTestBase<Activity_Login> {
    public TestLondonBusStop() {
        super(Activity_Login.class);
    }

    public void testMenuComponent() {
        assertTrue("Login Activity never started", solo.waitForActivity(Activity_Login.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        assertTrue("Couldn't find the Welcome Message", solo.searchText("Welcome To London"));
        assertTrue("Couldn't find the Author", solo.searchText("Hosted By: EvilZ"));

        login();
        assertTrue("Login Bus Stop Activity Expected", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        View shareView = solo.getView(R.id.action_share);
        assertTrue("Couldn't find the Share", shareView != null);
        solo.clickOnView(shareView);
        assertTrue("Login Bus Stop Activity Expected", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        View rateView = solo.getView(R.id.action_rate);
        assertTrue("Couldn't find the Share", rateView != null);
        solo.clickOnView(rateView);
        assertTrue("Login Bus Stop Activity Expected", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        solo.clickOnMenuItem("About Author");
        assertTrue("Where is My Fragment", solo.waitForFragmentByTag("Fragment_About_Author"));

        solo.clickOnMenuItem("Donate");
        assertTrue("Login Payment Activity Expected", solo.waitForActivity(PaymentActivity.class, WAIT_FOR_ACTIVITY_TIMEOUT));
        solo.goBack();
        assertTrue("Login Bus Stop Activity Expected", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        logout();
    }

    public void login(){
        SignInButton signInButton = (SignInButton) solo.getView(R.id.activity_login_btn_sign_in);
        assertTrue("Could not find the Sign In Button", signInButton != null);
        solo.clickOnView(signInButton);
    }

    public void logout(){
        solo.clickOnMenuItem("Sign Out");
        assertTrue("Where is My Activity", solo.waitForActivity(Activity_Login.class, WAIT_FOR_ACTIVITY_TIMEOUT));
    }
}
