package work.evilz.londonbusstop;

import android.support.design.widget.NavigationView;

import com.google.android.gms.common.SignInButton;

import work.evilz.londonbusstop.activities.Activity_Login;
import work.evilz.londonbusstop.activities.Activity_London_Bus_Stops;

/**
 * test Activity_London_Bus_Stops2
 *
 * @author ZeNan Gao
 * @version 09/01/2015
 * @since 1.0
 */
public class TestLondonBusStop2 extends AbstractSystemTestBase {
    public TestLondonBusStop2() {
        super(Activity_Login.class);
    }

    public void login(){
        SignInButton signInButton = (SignInButton) solo.getView(R.id.activity_login_btn_sign_in);
        assertTrue("Could not find the Sign In Button", signInButton != null);
        solo.clickOnView(signInButton);
    }

    public void logout(){
        solo.clickOnMenuItem("Sign Out");
        assertTrue("Where is My Activity", solo.waitForActivity(Activity_Login.class, WAIT_FOR_ACTIVITY_TIMEOUT));
    }

    public void testNavigationDrawer(){
        /*
        * Method 1: solo.clickOnImageButton(0);
        * Method 2:
        *   solo.setNavigationDrawer(Solo.OPENED);
        *   solo.clickOnActionBarHomeButton();
        */
        login();
        assertTrue("Login Bus Stop Activity Expected", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        NavigationView view = (NavigationView) solo.getView(R.id.navigation_view);
        assertTrue("Couldn't find the Drawer", view != null);

        /*
        solo.clickOnImageButton(0);
        View listView = view.findViewById(R.id.home_screen);
        solo.clickOnView(listView);
        assertTrue("Where is My Fragment", solo.waitForFragmentByTag("Fragment_Show_Saved_Stop"));

        solo.clickOnImageButton(0);
        View systemVariablesView = solo.getView(R.id.system_variable);
        assertTrue("Couldn't find Home", systemVariablesView != null);
        solo.clickOnView(systemVariablesView);
        assertTrue("Where is My System Variables Alert Dialog", solo.searchText("System Variables"));
        solo.clickOnButton("OK!");
        assertTrue("Where is My Activity", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        solo.clickOnImageButton(0);
        View currentLocationView = solo.getView(R.id.location);
        assertTrue("Couldn't find Home", currentLocationView != null);
        solo.clickOnView(currentLocationView);
        assertTrue("Where is My Current Location Alert Dialog", solo.searchText("Current Location"));
        solo.clickOnButton("OK");
        assertTrue("Where is My Activity", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));

        solo.clickOnImageButton(0);
        currentLocationView = solo.getView(R.id.location);
        solo.clickOnView(currentLocationView);
        assertTrue("Where is My Current Location Alert Dialog", solo.searchText("Current Location"));
        solo.clickOnButton("NAVIGATE TO MIAMI BEACH");
        assertTrue("Where is My Activity", solo.waitForActivity(Activity_London_Bus_Stops.class, WAIT_FOR_ACTIVITY_TIMEOUT));
        */
        logout();
    }
}
