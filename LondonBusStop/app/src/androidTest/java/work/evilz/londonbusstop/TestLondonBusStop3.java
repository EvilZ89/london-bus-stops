package work.evilz.londonbusstop;

import com.google.android.gms.common.SignInButton;

import work.evilz.londonbusstop.activities.Activity_Login;

/**
 * test Activity_London_Bus_Stops3
 *
 * @author ZeNan Gao
 * @version 09/01/2015
 * @since 1.0
 */
public class TestLondonBusStop3 extends AbstractSystemTestBase {
    public TestLondonBusStop3() {
        super(Activity_Login.class);
    }

    public void login(){
        SignInButton signInButton = (SignInButton) solo.getView(R.id.activity_login_btn_sign_in);
        assertTrue("Could not find the Sign In Button", signInButton != null);
        solo.clickOnView(signInButton);
    }

    public void logout(){
        solo.clickOnMenuItem("Sign Out");
        assertTrue("Where is My Activity", solo.waitForActivity(Activity_Login.class, WAIT_FOR_ACTIVITY_TIMEOUT));
    }

    public void testFragment(){
        login();
        assertTrue("Fragment Saved Stop expected", solo.waitForFragmentByTag("Fragment_Show_Saved_Stop"));

        /*
        solo.clickOnButton("SHOW NEARBY BUS STOPS");
        assertTrue("Fragment Nearby Stop expected", solo.waitForFragmentByTag("Fragment_Nearby_Stop"));

        solo.clickInRecyclerView(0);
        assertTrue("Fragment Show Stop Departure expected", solo.waitForFragmentByTag("Fragment_Show_Stop_Departure"));
        */

        logout();
    }
}
