package work.evilz.londonbusstop.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.SignInButton;

import io.fabric.sdk.android.Fabric;
import work.evilz.londonbusstop.authentications.GoogleLogin;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.constants.Constants;
import work.evilz.londonbusstop.push_notification.GcmRegistrationAsyncTask;

/**
 * loading activity for login
 * @author ZeNan Gao
 * @version 09/04/2015
 * @since 1.3
 */
public class Activity_Login extends AppCompatActivity implements
        View.OnClickListener{
    // google+ sign in
    private SignInButton mBTNSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.layout_activity_login);

        mBTNSignIn = (SignInButton) findViewById(R.id.activity_login_btn_sign_in);
        mBTNSignIn.setOnClickListener(this);

        // start GCM
        new GcmRegistrationAsyncTask(this).execute();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(mResolveErrorBR, mResolveErrorIF);
        registerReceiver(mLoginBR, mLoginIF);
        GoogleLogin.getInstance(this.getApplicationContext()).connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mResolveErrorBR);
        unregisterReceiver(mLoginBR);
    }

    private BroadcastReceiver mResolveErrorBR  = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // handle intent
            try {
                // Send the pending intent that we stored on the most recent
                // OnConnectionFailed callback.  This will allow the user to
                // resolve the error currently preventing our connection to
                // Google Play services.
                GoogleLogin.getInstance(getApplicationContext()).mSignInProgress =
                        GoogleLogin.STATE_IN_PROGRESS;
                startIntentSenderForResult(GoogleLogin.getInstance(getApplicationContext()).mSignInIntent.getIntentSender(),
                        GoogleLogin.RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                Log.i(GoogleLogin.TAG, "Sign in intent could not be sent: "
                        + e.getLocalizedMessage());
                // The intent was canceled before it was sent.  Attempt to connect to
                // get an updated ConnectionResult.
                GoogleLogin.getInstance(getApplicationContext()).mSignInProgress = GoogleLogin.STATE_SIGN_IN;
                GoogleLogin.getInstance(getApplicationContext()).connect();
            }
        }
    };
    private IntentFilter mResolveErrorIF = new IntentFilter(Constants.CUSTOM_INTENT1);

    private BroadcastReceiver mLoginBR  = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // handle intent
            Intent loginIntent = new Intent(Activity_Login.this, Activity_London_Bus_Stops.class);
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loginIntent);
            finish();
        }
    };
    private IntentFilter mLoginIF = new IntentFilter(Constants.CUSTOM_INTENT2);

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        switch (requestCode) {
            case GoogleLogin.RC_SIGN_IN:
                if (resultCode == RESULT_OK) {
                    // If the error resolution was successful we should continue
                    // processing errors.
                    GoogleLogin.getInstance(getApplicationContext()).mSignInProgress = GoogleLogin.STATE_SIGN_IN;
                } else {
                    // If the error resolution was not successful or the user canceled,
                    // we should stop processing errors.
                    GoogleLogin.getInstance(getApplicationContext()).mSignInProgress = GoogleLogin.STATE_DEFAULT;
                }

                if (!GoogleLogin.getInstance(getApplicationContext()).mGoogleApiClient.isConnecting()) {
                    // If Google Play services resolved the issue with a dialog then
                    // onStart is not called so we need to re-attempt connection here.
                    GoogleLogin.getInstance(getApplicationContext()).connect();
                }
                break;
        }
    }

    /**
     * Button onClick listener
     * */
    @Override
    public void onClick(View v) {
        if (!GoogleLogin.getInstance(getApplicationContext()).mGoogleApiClient.isConnecting()) {
            // We only process button clicks when GoogleApiClient is not transitioning
            // between connected and not connected.
            switch (v.getId()) {
                case R.id.activity_login_btn_sign_in:
                    // Login button clicked
                    GoogleLogin.getInstance(getApplicationContext()).signInWithGPlus();
                    break;
            }
        }
    }
}
