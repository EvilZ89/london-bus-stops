package work.evilz.londonbusstop.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.squareup.picasso.Picasso;

import work.evilz.londonbusstop.authentications.GoogleLogin;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.analytics.AnalyticsTrackers;
import work.evilz.londonbusstop.controllers.Controller_Activity_London_Bus_Stops;

import static work.evilz.londonbusstop.constants.Constants.mShareBody;
import static work.evilz.londonbusstop.constants.Constants.mShareSubject;

/**
 * view of MVC pattern, contain drawer layout
 * @author ZeNan Gao
 * @version 09/06/2015
 * @since 1.5
 */
public class Activity_London_Bus_Stops extends AppCompatActivity{
    public Controller_Activity_London_Bus_Stops controller;
    public Toolbar mToolbar;
    public DrawerLayout  mDrawerLayout;
    public NavigationView mNavigationView;
    private ShareActionProvider mShareActionProvider;
    private static final int PROFILE_PIC_SIZE = 400;
    private String mHeaderName, mHeaderImageUrl, mHeaderEmail;
    private boolean mFirstLoad;
    private de.hdodenhof.circleimageview.CircleImageView mUserImageView;
    private TextView mTVUsername, mTVEmail;
    private static final String TAG_IMAGE = "image";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_EMAIL = "email";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity__london_bus_stops);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setLogo(R.mipmap.ic_london_bus);

        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);
        controller = new Controller_Activity_London_Bus_Stops(this);
        controller.startExecution(savedInstanceState);
        mFirstLoad = true;
        mUserImageView = (de.hdodenhof.circleimageview.CircleImageView)
                        findViewById(R.id.profile_image);
        mTVUsername = (TextView)findViewById(R.id.username);
        mTVEmail = (TextView) findViewById(R.id.user_email);
        if (savedInstanceState != null) {
            mHeaderEmail = savedInstanceState.getString(TAG_EMAIL);
            mHeaderImageUrl = savedInstanceState.getString(TAG_IMAGE);
            mHeaderName = savedInstanceState.getString(TAG_USERNAME);
            loadHeader();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity__london_bus__stops, menu);
        MenuItem shareItem = menu.findItem(R.id.action_share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getDefaultIntent());
        return true;
    }

    /**
     * define a default share intent to initialize the action provider
     * @return share intent to be used
     */
    private Intent getDefaultIntent() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, mShareSubject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, mShareBody);
        return sharingIntent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_author:
                controller.showAuthorInfo();
                sentTracker(item);
                return true;
            case R.id.action_donate:
                controller.donate();
                sentTracker(item);
                return true;
            case R.id.action_logout:
                signOutFromGPlus();
                sentTracker(item);
                return true;
            case R.id.action_rate:
                controller.rate();
                sentTracker(item);
                return true;
            case R.id.action_share:
                sentTracker(item);
                return true;
            case R.id.action_force_crash:
                sentTracker(item);
                controller.forceCrash();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * process paypal request
     * @param requestCode request code for PayPal
     * @param resultCode result code for PayPal
     * @param data confirmation data for PayPal
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        controller.processPayPal(requestCode, requestCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        controller.connectGoogleAPIClient();
        GoogleLogin.getInstance(getApplicationContext()).connect();
        setDrawerHeader();
    }

    @Override
    protected void onStop() {
        super.onStop();
        controller.disconnectGoogleAPIClient();
        GoogleLogin.getInstance(getApplicationContext()).disconnect();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        controller.saveInstance(outState);
        outState.putString(TAG_USERNAME, mHeaderName);
        outState.putString(TAG_EMAIL, mHeaderEmail);
        outState.putString(TAG_IMAGE, mHeaderImageUrl);
        super.onSaveInstanceState(outState);
    }

    /**
     * stop connection of Google+ Login and return to Login Screen
     */
    public void signOutFromGPlus() {
        Log.e("Status", "Sign Out");
        if (GoogleLogin.getInstance(getApplicationContext()).mGoogleApiClient.isConnected()) {
            GoogleLogin.getInstance(getApplicationContext()).mSignInProgress = GoogleLogin.STATE_DEFAULT;
            GoogleLogin.getInstance(getApplicationContext()).mSignedIn = false;
            Plus.AccountApi.clearDefaultAccount(GoogleLogin.getInstance(getApplicationContext()).mGoogleApiClient);
            GoogleLogin.getInstance(getApplicationContext()).disconnect();
        }
        Intent intent = new Intent(this, Activity_Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * setting drawer header view using google profile
     */
    private void setDrawerHeader() {
        // drawer header view
        if (mFirstLoad == true) {
            try {
                if (Plus.PeopleApi.getCurrentPerson(GoogleLogin.getInstance(getApplicationContext()).mGoogleApiClient) != null) {
                    mFirstLoad = false;
                    Person currentPerson = Plus.PeopleApi
                            .getCurrentPerson(GoogleLogin.getInstance(getApplicationContext()).mGoogleApiClient);
                    mHeaderName = currentPerson.getDisplayName();
                    mHeaderImageUrl = currentPerson.getImage().getUrl();
                    //String profile = currentPerson.getUrl();
                    mHeaderEmail = Plus.AccountApi.getAccountName(GoogleLogin.getInstance(getApplicationContext()).mGoogleApiClient);
                    mHeaderImageUrl = mHeaderImageUrl.substring(0, mHeaderImageUrl.length() - 2) + PROFILE_PIC_SIZE;
                    loadHeader();
                } else {
                    Toast.makeText(this, "Person information is null", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * loading header
     */
    private void loadHeader() {
        mTVUsername.setText(mHeaderName);
        mTVEmail.setText(mHeaderEmail);
        Picasso.with(this)
                .load(Uri.parse(mHeaderImageUrl))
                .into(mUserImageView);
    }
    /**
     * sending tracking to Google
     * @param item menu item where the info is obtained
     */
    public void sentTracker(MenuItem item) {
        String title = item.getTitle().toString();
        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(this).get(
                AnalyticsTrackers.Target.APP);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Main Activity")
                .setAction("Menu Setting Clicked")
                .setLabel(title)
                .build());
    }
}
