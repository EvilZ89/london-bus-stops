package work.evilz.londonbusstop.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import work.evilz.londonbusstop.helpers.BusStopSQLiteHelper;
import work.evilz.londonbusstop.helpers.BusStopBasicInfo;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.activities.Activity_London_Bus_Stops;
import work.evilz.londonbusstop.helpers.BusStopInfo;

import static work.evilz.londonbusstop.constants.Constants.mStringSeparator;
import static work.evilz.londonbusstop.constants.Constants.Tag_atcocode;
import static work.evilz.londonbusstop.constants.Constants.Tag_bearing;
import static work.evilz.londonbusstop.constants.Constants.Tag_distance;
import static work.evilz.londonbusstop.constants.Constants.Tag_indicator;
import static work.evilz.londonbusstop.constants.Constants.Tag_locality;
import static work.evilz.londonbusstop.constants.Constants.Tag_name;
import static work.evilz.londonbusstop.constants.Constants.mUnitDistance;

/**
 * adapter to be used with Recycler view and Card view
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class BusStopAdapter extends RecyclerView.Adapter<BusStopViewHolder> {

    private ArrayList<BusStopInfo> mBusStopList;
    private Activity act;
    //REMEMBER: THERE ARE FOR METHODS TO BE ALWAYS MENTIONED IN THIS CLASS OF ADAPTER
    // 1. Constructor of the class
    // 2. onCreateViewHolder(): specify the row layout file and click for each row
    // 3. onBindViewHolder(): load data in each row element
    // 4. getItemCount(): check the size

    /**
     * Constructor of the class for initialize data and associating activity
     * @param act associating activity for which recycler view will be displayed on
     * @param list data that will be displayed in recycler view
     */
    public BusStopAdapter(Activity act, ArrayList<BusStopInfo> list) {
        this.act = act;
        this.mBusStopList = list;
    }

    /**
     * inflate layout
     * @param parent recycler view group
     * @param viewType not used
     * @return view holder
     */
    @Override
    public BusStopViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_bus_stops, parent, false);
        return new BusStopViewHolder(view);
    }

    /**
     * binds data to views
     * @param holder card view holder
     * @param listPosition position of the card related to the recycler view
     */
    @Override
    public void onBindViewHolder(final BusStopViewHolder holder, final int listPosition) {

        // link with card view holder
        final TextView TV_atcocode = holder.atcocode;
        final TextView TV_name = holder.name;
        final TextView TV_bearing = holder.bearing;
        final TextView TV_locality = holder.locality;
        final TextView TV_indicator = holder.indicator;
        final TextView TV_distance = holder.distance;
        final ImageButton IB_favorite = holder.favorite;

        // getting information from data
        final BusStopInfo mBusStopInfo = mBusStopList.get(listPosition);
        final String atcocode = mBusStopInfo.getAtcocode();
        final String name = mBusStopInfo.getName();
        final String bearing = mBusStopInfo.getBearing();
        final String locality = mBusStopInfo.getLocality();
        final String indicator = mBusStopInfo.getIndicator();
        final int distance = mBusStopInfo.getDistance();

        // setting image button image according whether its record exist in database
        BusStopSQLiteHelper dbHandler = new BusStopSQLiteHelper(act);
        BusStopBasicInfo info = dbHandler.Get_Single_Stop(atcocode);
        if (info == null) {
            IB_favorite.setImageResource(R.drawable.ic_favorite_unselected);
        } else {
            IB_favorite.setImageResource(R.drawable.ic_favorite_selected);
        }
        dbHandler.close();

        // setting information to be shown in card view
        TV_atcocode.setText(Tag_atcocode + mStringSeparator + atcocode);
        TV_name.setText(Tag_name + mStringSeparator + name);
        TV_bearing.setText(Tag_bearing + mStringSeparator + bearing);
        TV_locality.setText(Tag_locality + mStringSeparator + locality);
        TV_indicator.setText(Tag_indicator + mStringSeparator + indicator);
        TV_distance.setText(Tag_distance + mStringSeparator + distance + mUnitDistance);

        // register listener for image button click for add/delete entry from database
        // and update image button images
        IB_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusStopSQLiteHelper dbHandler = new BusStopSQLiteHelper(act);
                BusStopBasicInfo info = dbHandler.Get_Single_Stop(atcocode);
                if (info == null) {
                    dbHandler.Add_BusStop(mBusStopInfo);
                    IB_favorite.setImageResource(R.drawable.ic_favorite_selected);
                } else {
                    dbHandler.Delete_Stop(atcocode);
                    IB_favorite.setImageResource(R.drawable.ic_favorite_unselected);
                }
                dbHandler.close();
            }
        });

        // register listener for card view click for showing departure time
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusStopBasicInfo info1 = new BusStopBasicInfo(atcocode, name, bearing,
                        locality, mBusStopInfo.getLatitude(), mBusStopInfo.getLongitude());
                ((Activity_London_Bus_Stops) act).controller.showStopDeparture(info1);
            }
        });
    }

    /**
     * to obtain size of data
     * @return item count of bus stop size
     */
    @Override
    public int getItemCount() {
        return mBusStopList.size();
    }
}
