package work.evilz.londonbusstop.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import work.evilz.londonbusstop.R;

/**
 * Recycler view holder class
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class BusStopViewHolder extends RecyclerView.ViewHolder {

    // declares view references to be stored
    // use butter knife to bind view holder
    @Bind(R.id.cardview_bus_stops_tv_atcocode)
    public TextView atcocode;
    @Bind(R.id.cardview_bus_stops_tv_name)
    public TextView name;
    @Bind(R.id.cardview_bus_stops_tv_bearing)
    public TextView bearing;
    @Bind(R.id.cardview_bus_stops_tv_locality)
    public TextView locality;
    @Bind(R.id.cardview_bus_stops_tv_indicator)
    public TextView indicator;
    @Bind(R.id.cardview_bus_stops_tv_distance)
    public TextView distance;
    @Bind(R.id.cardview_bus_stops_btn_image)
    public ImageButton favorite;
    public View view;

    /**
     * Look up and stores view references
     * @param itemView view where references can be located
     */
    public BusStopViewHolder(View itemView) {
        super(itemView);
        // initialize Butter knife
        ButterKnife.bind(this,itemView);
        view = itemView;
    }
}
