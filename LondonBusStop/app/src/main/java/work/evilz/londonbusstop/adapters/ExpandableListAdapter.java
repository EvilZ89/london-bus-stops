package work.evilz.londonbusstop.adapters;

/**
 * Adapter used with Expandable list view
 * @link http://www.androidhive.info/2013/07/android-expandable-list-view-tutorial/
 * @author ZeNan Gao
 * @version 08/26/2015
 * @since 1.1
 */
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import work.evilz.londonbusstop.R;
import static work.evilz.londonbusstop.constants.Constants.*;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<String>> _listDataChild;

    /**
     * constructor for setting data to be displayed in Expandable list
     * @param context context where data will be displayed on
     * @param listDataHeader list of data headers
     * @param listChildData list of child data for all data headers
     */
    public ExpandableListAdapter(Context context, ArrayList<String> listDataHeader,
                                 HashMap<String, ArrayList<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    /**
     * get child at specific group position and child position
     * @param groupPosition group position relative to Expandable list header
     * @param childPosition child position relative to each header
     * @return child object at given group position and child position
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition);
    }

    /**
     * getting child id
     * @param groupPosition group position relative to Expandable list header
     * @param childPosition child position relative to each header
     * @return child id (child position)
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    /**
     * get child view
     * @param groupPosition group position relative to Expandable list header
     * @param childPosition child position relative to each header
     * @param isLastChild not used
     * @param convertView view where each child will be displayed on
     * @param parent not used; the parent that this view will eventually be attached to
     * @return child view
     */
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(mDepartureHeader + childText);
        return convertView;
    }

    /**
     * get child count at given group position
     * @param groupPosition group position relative to Expandable list header
     * @return get child count at given group position
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    /**
     * getting group header
     * @param groupPosition group position relative to Expandable list header
     * @return group header in expandable list
     */
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    /**
     * getting group header count
     * @return group header count
     */
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    /**
     * getting group position relative to Expandable list header
     * @param groupPosition group position relative to Expandable list header
     * @return group position
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * get view where list header will be displayed on
     * @param groupPosition group position relative to Expandable list header
     * @param isExpanded not used
     * @param convertView view where list header will be displayed on
     * @param parent not used; the parent that this view will eventually be attached to
     * @return view where list header will be displayed on
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(mBusLineHeader + headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * make child at given group position and child position selectable
     * @param groupPosition group position relative to Expandable list header
     * @param childPosition child position relative to each header
     * @return whether child at given group position and child position is selectable or not
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
