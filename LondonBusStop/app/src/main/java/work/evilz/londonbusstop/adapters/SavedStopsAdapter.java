package work.evilz.londonbusstop.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.activities.Activity_London_Bus_Stops;
import work.evilz.londonbusstop.fragments.Fragment_Saved_Stops;
import work.evilz.londonbusstop.helpers.BusStopBasicInfo;
import work.evilz.londonbusstop.helpers.BusStopSQLiteHelper;

/**
 * Adapter class to be used for list view
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.0
 */
public class SavedStopsAdapter extends ArrayAdapter<BusStopBasicInfo> {

    private Fragment_Saved_Stops mFragment;
    private int layoutResourceId;
    private ArrayList<BusStopBasicInfo> data = new ArrayList<>();

    /**
     * Constructor for saving calling fragment, layout resource id for list view
     * and list view data, update list view using notifyDataSetChanged()
     * @param fragment calling fragment
     * @param layoutResourceId layout resource id for list view
     * @param data list view data
     */
    public SavedStopsAdapter(Fragment_Saved_Stops fragment, int layoutResourceId,
                             ArrayList<BusStopBasicInfo> data) {
        super(fragment.getActivity(), layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mFragment = fragment;
        this.data = data;
        notifyDataSetChanged();
    }

    /**
     * inflate view, display data on each list item via inner holder class
     * @param position position in the list view
     * @param rowView row view for holder to be displayed on
     * @param parent the parent that this view will eventually be attached to
     * @return rowView
     */
    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        StopHolder holder;
        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(mFragment.getActivity());
            rowView = inflater.inflate(layoutResourceId, parent, false);

            // link java objects with view elements
            holder = new StopHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (StopHolder) rowView.getTag();
        }

        // set holder fields
        final BusStopBasicInfo mStop = data.get(position);
        holder.show_departure.setTag(mStop.getAtcocode());
        holder.delete.setTag(mStop.getAtcocode());
        holder.name.setText("Name: " + mStop.getName());
        holder.bearing.setText("Bearing: " + mStop.getBearing());
        holder.locality.setText("Locality: " + mStop.getLocality());

        // register holder button listeners
        // show bus stop departure times of a particular saved stop
        holder.show_departure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity_London_Bus_Stops) mFragment.getActivity()).controller.showStopDeparture(mStop);
            }
        });
        // delete item from database and update the image button
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder adb = new AlertDialog.Builder(mFragment.getActivity());
                adb.setTitle("Delete?");
                adb.setMessage("Are you sure you want to delete ");
                final String atcocode = v.getTag().toString();
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Ok",
                        new AlertDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                BusStopSQLiteHelper dBHandler = new BusStopSQLiteHelper(
                                        mFragment.getActivity().getApplicationContext());
                                dBHandler.Delete_Stop(atcocode);
                                mFragment.refreshListView();
                                dBHandler.close();
                            }
                        });
                adb.show();
            }
        });
        return rowView;
    }

    /**
     * Static inner holder class
     */
    static class StopHolder {
        // use butter knife to bind  view holder
        @Bind(R.id.listview_saved_stops_tv_name)
        TextView name;
        @Bind(R.id.listview_saved_stops_tv_bearing)
        TextView bearing;
        @Bind(R.id.listview_saved_stops_tv_locality)
        TextView locality;
        @Bind(R.id.listview_saved_stops_btn_show_departure)
        Button show_departure;
        @Bind(R.id.listview_saved_stops_btn_delete)
        Button delete;

        public StopHolder(View view) {
            // initialize Butter knife
            ButterKnife.bind(this, view);
        }
    }
}
