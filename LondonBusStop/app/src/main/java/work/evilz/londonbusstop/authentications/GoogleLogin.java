package work.evilz.londonbusstop.authentications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import work.evilz.londonbusstop.system_utils.CheckNetworkConnection;
import work.evilz.londonbusstop.constants.Constants;

/**
 * singleton class for google+ signin
 * @author ZeNan Gao
 * @version 09/04/2015
 * @since 1.1
 */
public class GoogleLogin implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private static GoogleLogin mInstance;
    private Context mContext;

    public static final String TAG = "android-plus-quickstart";

    public static final int STATE_DEFAULT = 0;
    public static final int STATE_SIGN_IN = 1;
    public static final int STATE_IN_PROGRESS = 2;

    public static final int RC_SIGN_IN = 0;

    public boolean mSignedIn;

    // GoogleApiClient wraps our service connection to Google Play services and
    // provides access to the users sign in state and Google's APIs.
    public GoogleApiClient mGoogleApiClient;

    // We use mSignInProgress to track whether user has clicked sign in.
    // mSignInProgress can be one of three values:
    //
    //       STATE_DEFAULT: The default state of the application before the user
    //                      has clicked 'sign in', or after they have clicked
    //                      'sign out'.  In this state we will not attempt to
    //                      resolve sign in errors and so will display our
    //                      Activity in a signed out state.
    //       STATE_SIGN_IN: This state indicates that the user has clicked 'sign
    //                      in', so resolve successive errors preventing sign in
    //                      until the user has successfully authorized an account
    //                      for our app.
    //   STATE_IN_PROGRESS: This state indicates that we have started an intent to
    //                      resolve an error, and so we should not start further
    //                      intents until the current intent completes.
    public int mSignInProgress;

    // Used to store the PendingIntent most recently returned by Google Play
    // services until the user clicks 'sign in'.
    public PendingIntent mSignInIntent;

    // Used to store the error code most recently returned by Google Play services
    // until the user clicks 'sign in'.
    public int mSignInError;

    /**
     * default constructor
     * @param context the context this google api client is associated with
     */
    private GoogleLogin(Context context) {
        mContext = context;
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
        mSignInProgress = STATE_DEFAULT;
        mSignedIn = false;
    }

    /**
     * create public entry point for this singleton class
     * @param context the context this google api client is associated with
     * @return singleton instance object
     */
    public static synchronized GoogleLogin getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new GoogleLogin(context);
        }
        return mInstance;
    }

    /**
     * connect api client
     */
    public void connect() {
        if (!mGoogleApiClient.isConnected()) {
            Log.e("Plus Login", "connect");
            mGoogleApiClient.connect();
        }
    }

    /**
     * disconnect api client
     */
    public void disconnect() {
        if (mGoogleApiClient.isConnected()) {
            Log.e("Plus Login", "disconnect");
            mGoogleApiClient.disconnect();
        }
    }

    /**
     *  onConnected is called when our Activity successfully connects to Google
     * Play services.  onConnected indicates that an account was selected on the
     * device, that the selected account has granted any requested permissions to
     * our app and that we were able to establish a service connection to Google
     * Play services.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Reaching onConnected means we consider the user signed in.
        // Log.i(TAG, mSignInProgress + " " + mSignInIntent.toString() + " " + mSignInError);
        Log.i("Status", "Sign In " + mSignedIn);
        mSignedIn = true;
        Intent in = new Intent();
        in.setAction(Constants.CUSTOM_INTENT2);
        mContext.sendBroadcast(in);

    }

    /**
     *  onConnectionFailed is called when our Activity could not connect to Google
     * Play services.  onConnectionFailed indicates that the user needs to select
     * an account, grant permissions or resolve an error in order to sign in.
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.i(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());

        if (result.getErrorCode() == ConnectionResult.API_UNAVAILABLE) {
            // An API requested for GoogleApiClient is not available. The device's current
            // configuration might not be supported with the requested API or a required component
            // may not be installed, such as the Android Wear application. You may need to use a
            // second GoogleApiClient to manage the application's optional APIs.
            Log.w(TAG, "API Unavailable.");
        } else if (mSignInProgress != STATE_IN_PROGRESS) {
            // We do not have an intent in progress so we should store the latest
            // error resolution intent for use when the sign in button is clicked.
            mSignInIntent = result.getResolution();
            mSignInError = result.getErrorCode();

            if (mSignInProgress == STATE_SIGN_IN) {
                // STATE_SIGN_IN indicates the user already clicked the sign in button
                // so we should continue processing errors until the user is signed in
                // or they click cancel.
                resolveSignInError();
            }
        }
    }

    /**
     *  Starts an appropriate intent or dialog for user interaction to resolve
     * the current error preventing the user from being signed in.  This could
     * be a dialog allowing the user to select an account, an activity allowing
     * the user to consent to the permissions being requested by your app, a
     * setting to enable device networking, etc.
     */
    private void resolveSignInError() {
        if (mSignInIntent != null) {
            // We have an intent which will allow our user to sign in or
            // resolve an error.  For example if the user needs to
            // select an account to sign in with, or if they need to consent
            // to the permissions your app is requesting.

            /*
            try {
                // Send the pending intent that we stored on the most recent
                // OnConnectionFailed callback.  This will allow the user to
                // resolve the error currently preventing our connection to
                // Google Play services.
                mSignInProgress = STATE_IN_PROGRESS;
                startIntentSenderForResult(mSignInIntent.getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                Log.i(TAG, "Sign in intent could not be sent: "
                        + e.getLocalizedMessage());
                // The intent was canceled before it was sent.  Attempt to connect to
                // get an updated ConnectionResult.
                mSignInProgress = STATE_SIGN_IN;
                mGoogleApiClient.connect();
            }
            */
            // use broadcast to initialize action in Login Activity
            Intent in = new Intent();
            in.setAction(Constants.CUSTOM_INTENT1);
            mContext.sendBroadcast(in);
        } else {
            // Google Play services wasn't able to provide an intent for some
            // error types, so we show the default Google Play services error
            // dialog which may still start an intent on our behalf if the
            // user can resolve the issue.
            Log.i(TAG, "Sign In Error Code: " + mSignInError);
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason.
        // We call connect() to attempt to re-establish the connection or get a
        // ConnectionResult that we can attempt to resolve.
        mGoogleApiClient.connect();
    }

    /**
     * Sign-in into google
     * */
    public void signInWithGPlus() {
        // check network status
        if (!CheckNetworkConnection.checkConnection(mContext)) {
            Toast.makeText(mContext, "Network not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mGoogleApiClient.isConnecting()) {
            mSignInProgress = STATE_SIGN_IN;
            mGoogleApiClient.connect();
        }
    }
}
