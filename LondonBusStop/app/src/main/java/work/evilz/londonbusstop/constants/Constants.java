package work.evilz.londonbusstop.constants;

/**
 * Class holding constant variables that are shared for all classes
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public final class Constants {
    // common constant strings
    public static final String mAuthorImageUrl = "http://evilz123.weebly.com/uploads/5/8/9/9/58993791/11834_orig.jpg";
    public static final String mUniversityUrl = "http://evilz123.weebly.com/uploads/5/8/9/9/58993791/278992_orig.png";
    public static final String mNetworkImageUrl1 = "http://evilz123.weebly.com/uploads/5/8/9/9/58993791/4403986.jpg";
    public static final String mNetworkImageUrl2 = "http://evilz123.weebly.com/uploads/5/8/9/9/58993791/2516279.jpg";
    public static final String mNetworkImageUrl3 = "http://evilz123.weebly.com/uploads/5/8/9/9/58993791/5097077.jpg";
    public static final String mNearbyStopsAPI = "http://transportapi.com/v3/uk/bus/stops/near.json?";
    public static final String mDepartureAPIHeader = "http://transportapi.com/v3/uk/bus/stop/";
    public static final String mDepartureAPIFooter = "/timetable.json?";
    public static final String mBusAPIKey = "39ed555c08bbfa8fcccbd3fdd9923c9e";
    public static final String mBusAppID = "fa2d652b";
    public static final String mAppString = "&app_id=";
    public static final String mKeyString = "&api_key=";
    public static final String mAuthToken = mKeyString + mBusAPIKey + mAppString + mBusAppID;
    public static final String mLatArg = "lat=";
    public static final String mLngArg = "&lon=";
    public static final double mFakeLat = 51.5;
    public static final double mFakeLng = -0.10;
    public static final String mUnitDistance = " meters";
    public static final String mStringSeparator = ": ";
    public static final String mBusLineHeader = "Bus Line # ";
    public static final String mDepartureHeader = "Departure Time: ";
    public static final String mCurrentPositionMsg = "You are Here";
    public static final String mCurrentDateTime = "Current Date-Time: ";
    public static final String mCurrentOSVersion = "Current OS Version ";
    public static final String mCurrentOSName = "Current OS Name: ";
    public static final String mConfirm = "OK!";
    public static final String mErrorTTSLanguageNotSupport = "This Language is not supported!";
    public static final String mErrorTTSInitializationFail = "Initialization Failed!";
    public static final String mShareBody = "London Bus Stop is a great App. Love it!!! :-)";
    public static final String mShareSubject = "Try London Bus Stop App";
    public static final String mShareViaMsg = "Share Via";
    public static final int mGoogleLogin = 1;
    public static final int mFacebookLogin = 2;
    public static final int mTwitterLogin = 3;

    // common constant Tags used as keys or strings
    public static final String Tag_stop = "stops";
    public static final String Tag_atcocode = "atcocode";
    public static final String Tag_smscode = "smscode";
    public static final String Tag_name = "name";
    public static final String Tag_bearing = "bearing";
    public static final String Tag_locality = "locality";
    public static final String Tag_indicator = "indicator";
    public static final String Tag_longitude = "longitude";
    public static final String Tag_latitude = "latitude";
    public static final String Tag_distance = "distance";
    public static final String Tag_departure = "departures";
    public static final String Tag_ELParentSize = "parent_size";
    public static final String Tag_ELChildList = "child_list";
    public static final String Tag_ELKey = "key";
    public static final String Tag_departure_time = "aimed_departure_time";
    public static final String Tag_BusStopInfo = "bus_stop_info";
    public static final String Tag_SharedPreference = "rate_preference";
    public static final String Tag_rate_count = "Rate_Count";

    // google location variables
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME = "work.evilz.londonbusstop";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final String TAG_FETCH_ADDRESS_INTENT_SERVICE = "Fetch Address";
    public static final String RESPONSE_RECEIVER_ERROR = "No receiver received. There is nowhere to send the results.";
    public static final String RESPONSE_DATA_ERROR = "No location data provided";
    public static final String RESPONSE_SERVICE_ERROR = "Sorry, the service  is not available";
    public static final String RESPONSE_LAT_LNG_ERROR = "Invalid latitude or longitude used.";
    public static final String RESPONSE_NULL_ADDRESS = "Sorry, no address found";
    public static final String ERROR_INTENT_SERVICE = "see logcat: " + TAG_FETCH_ADDRESS_INTENT_SERVICE;
    public static final String MESSAGE_SUCCESS = "Address Found";
    public static final String ADDRESS_REQUESTED_KEY = "address-request-pending";
    public static final String LOCATION_ADDRESS_KEY = "location-address";
    public static final String ERROR_NO_GEOCODER = "No Geocoder Found";

    // custom Intents
    public static final String CUSTOM_INTENT1 = "Resolve-Error-for-Google-Login";
    public static final String CUSTOM_INTENT2 = "Start-Login";
}
