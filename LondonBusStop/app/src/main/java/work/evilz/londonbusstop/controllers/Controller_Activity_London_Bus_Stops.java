package work.evilz.londonbusstop.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

import work.evilz.londonbusstop.system_utils.CheckNetworkConnection;
import work.evilz.londonbusstop.analytics.AnalyticsTrackers;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.activities.Activity_London_Bus_Stops;
import work.evilz.londonbusstop.constants.Constants;
import work.evilz.londonbusstop.helpers.BusStopBasicInfo;
import work.evilz.londonbusstop.models.Model_Activity_London_Bus_Stops;
import work.evilz.londonbusstop.services.FetchAddressIntentService;

import static work.evilz.londonbusstop.constants.Constants.ADDRESS_REQUESTED_KEY;
import static work.evilz.londonbusstop.constants.Constants.ERROR_NO_GEOCODER;
import static work.evilz.londonbusstop.constants.Constants.LOCATION_ADDRESS_KEY;
import static work.evilz.londonbusstop.constants.Constants.LOCATION_DATA_EXTRA;
import static work.evilz.londonbusstop.constants.Constants.MESSAGE_SUCCESS;
import static work.evilz.londonbusstop.constants.Constants.RECEIVER;
import static work.evilz.londonbusstop.constants.Constants.TAG_FETCH_ADDRESS_INTENT_SERVICE;
/**
 * Controller in MVC pattern
 * For a starter example that displays the last known location of a device using a longitude and latitude,
 * see https://github.com/googlesamples/android-play-location/tree/master/BasicLocation.
 *
 * For an example that shows location updates using the Fused Location Provider API, see
 * https://github.com/googlesamples/android-play-location/tree/master/LocationUpdates.
 *
 * This sample uses Google Play services (GoogleApiClient) but does not need to authenticate a user.
 * For an example that uses authentication, see
 * https://github.com/googlesamples/android-google-accounts/tree/master/QuickStart.
 * @author ZeNan Gao
 * @version 09/06/2015
 * @since 1.5
 */
public class Controller_Activity_London_Bus_Stops implements
        NavigationView.OnNavigationItemSelectedListener, ConnectionCallbacks, OnConnectionFailedListener{
    // objects of model and view in MVC pattern
    public Activity_London_Bus_Stops view;
    private Model_Activity_London_Bus_Stops model;

    // PayPal variables
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final String TAG = "PayPal Payment";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AWG8zgCDJmlZyfDc3Fdd3eUhJ2vfDabEOZ7hwhdVtwyHlnmQazvTCXg6hGECGU5nheXRBVk14h-opJaJ";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("EvilZ's Evil Corp.")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    private Intent intent = new Intent();

    // Google Api variables for fetching  address
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;
    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    /**
     * Tracks whether the user has requested an address. Becomes true when the user requests an
     * address and false when the address (or an error message) is delivered.
     * The user requests an address by pressing the Fetch Address button. This may happen
     * before GoogleApiClient connects. This activity uses this boolean to keep track of the
     * user's intent. If the value is true, the activity tries to fetch the address as soon as
     * GoogleApiClient connects.
     */
    protected boolean mAddressRequested;
    /**
     * The formatted location address.
     */
    protected String mAddressOutput;
    /**
     * Receiver registered with this activity to get the response from FetchAddressIntentService.
     */
    private AddressResultReceiver mResultReceiver;

    /**
     * save view object and initialize model object
     * @param view view of MCV pattern
     */
    public Controller_Activity_London_Bus_Stops(Activity_London_Bus_Stops view){
        this.view = view;
        model = new Model_Activity_London_Bus_Stops(this);
    }

    /**
     * register event listeners
     */
    public void startExecution(Bundle bd){

        // register navigation view listener
        view.mNavigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(view,
                view.mDrawerLayout, view.mToolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open
                super.onDrawerOpened(drawerView);
            }
        };
        // Setting the actionbarToggle to drawer layout
        view.mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        // calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        // if activity saveInstance is not null, displayed the initial view in fragment container
        // otherwise, not load initial view
        // doing so persist fragment inside container after screen orientation change
        if (bd == null) {
            model.showSavedStops();
        }

        // start paypal Intent
        Intent intent = new Intent(view, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        view.startService(intent);

        // google api intent service receiver
        buildGoogleApiClient();
        mResultReceiver = new AddressResultReceiver(new Handler());
        /*
        if (bd != null) {
            // Check savedInstanceState to see if the address was previously requested.
            if (bd.keySet().contains(ADDRESS_REQUESTED_KEY)) {
                mAddressRequested = bd.getBoolean(ADDRESS_REQUESTED_KEY);
            }
            // Check savedInstanceState to see if the location address string was previously found
            // and stored in the Bundle. If it was found, display the address string in the UI.
            if (bd.keySet().contains(LOCATION_ADDRESS_KEY)) {
                mAddressOutput = bd.getString(LOCATION_ADDRESS_KEY);
                displayAddressOutput();
            }
        }*/
    }

    /**
     * event handler based on menuItem.getItemId()
     * @param menuItem menu
     * @return True if the event is handled, false otherwise
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        if (menuItem.isChecked()) menuItem.setChecked(false);
        else menuItem.setChecked(true);

        //Closing drawer on item click
        view.mDrawerLayout.closeDrawers();

        //Check to see which item was being clicked and perform appropriate action
        switch (menuItem.getItemId()) {
            case R.id.home_screen:
                model.showSavedStops();
                menuItem.setChecked(false);
                sentAnalytics(menuItem);
                return true;
            case R.id.system_variable:
                model.showCurrentSystemVariables();
                sentAnalytics(menuItem);
                menuItem.setChecked(false);
                return true;
            case R.id.location:
                showCurrentLocation();
                sentAnalytics(menuItem);
                menuItem.setChecked(false);
                return true;
            default:
                Toast.makeText(view, menuItem.getItemId(), Toast.LENGTH_SHORT).show();
                return true;
        }
    }

    /**
     * show Author Fragment inside Container
     */
    public void showAuthorInfo(){
        model.showAuthorInfo();
    }

    /**
     * show NearbyStop Fragment inside container
     */
    public void showNearbyStop() {
        model.showNearbyStops();
    }

    /**
     * show BusStopDeparture Fragment inside container for particular bus stop
     * @param info bus stop info
     */
    public void showStopDeparture(BusStopBasicInfo info) {
        model.showStopDeparture(info);
    }

    /**
     * action donate
     */
    public void donate() {
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(5.99), "USD", "EvilZ's Evil Love",
                PayPalPayment.PAYMENT_INTENT_SALE);

        intent = new Intent(view, PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        view.startActivityForResult(intent,
                Controller_Activity_London_Bus_Stops.REQUEST_CODE_PAYMENT);
    }

    /**
     * action rate app
     */
    public void rate() {
        model.rateApp();
    }

    /**
     * process paypal request
     * @param requestCode request code for PayPal
     * @param resultCode result code for PayPal
     * @param data confirmation data for PayPal
     */
    public void processPayPal(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, requestCode + " " + resultCode);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_FIRST_USER || resultCode == Activity.RESULT_OK) {
                if(data != null) {
                    PaymentConfirmation confirm = data
                            .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                    if (confirm != null) {
                        try {
                            Log.i(TAG, confirm.toJSONObject().toString(4));
                            Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /*
                         * send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                            Toast.makeText(view,
                                    "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
                                    .show();
                        } catch (JSONException e) {
                            Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                        }
                    }
                }else {
                    Log.i(TAG, "The user canceled.");
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            Log.i(TAG, "Future Request");
        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            Log.i(TAG, "Profile Sharing");
        }
    }

    /**
     * Builds a GoogleApiClient. Uses {@code #addApi} to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(view)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * start google api client on activity OnStart()
     */
    public void connectGoogleAPIClient() {
        mGoogleApiClient.connect();
    }

    /**
     * disconnect google api client on activity OnStop()
     */
    public void disconnectGoogleAPIClient(){
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            // Determine whether a Geocoder is available.
            if (!Geocoder.isPresent()) {
                Toast.makeText(view, ERROR_NO_GEOCODER, Toast.LENGTH_LONG).show();
                return;
            }
            // It is possible that the user presses the button to get the address before the
            // GoogleApiClient object successfully connects. In such a case, mAddressRequested
            // is set to true, but no attempt is made to fetch the address (see
            // fetchAddressButtonHandler()) . Instead, we start the intent service here if the
            // user has requested an address, since we now have a connection to GoogleApiClient.
            if (mAddressRequested) {
                startIntentService();
            }
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService() {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent mFetchAddressIntent = new Intent(view, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        mFetchAddressIntent.putExtra(RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        mFetchAddressIntent.putExtra(LOCATION_DATA_EXTRA, mLastLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        view.startService(mFetchAddressIntent);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG_FETCH_ADDRESS_INTENT_SERVICE, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG_FETCH_ADDRESS_INTENT_SERVICE, "Connection suspended");
        mGoogleApiClient.connect();
    }

    /**
     * save location request on activity SaveInstanceState(bundle)
     * @param bundle bundle to hold request
     */
    public void saveInstance(Bundle bundle) {
        // Save whether the address has been requested.
        bundle.putBoolean(ADDRESS_REQUESTED_KEY, mAddressRequested);

        // Save the address string.
        bundle.putString(LOCATION_ADDRESS_KEY, mAddressOutput);
    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         *  Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
                Toast.makeText(view, MESSAGE_SUCCESS, Toast.LENGTH_SHORT).show();
            }

            // Reset. Enable the Fetch Address button and stop showing the progress bar.
            mAddressRequested = false;
        }
    }

    /**
     * Display Location Result using alert dialog
     * navigate to miami beach if user desire\
     */
    private void displayAddressOutput(){
        AlertDialog.Builder adb = new AlertDialog.Builder(view);
        adb.setTitle("Current Location");
        adb.setMessage(mAddressOutput);
        adb.setNegativeButton("OK", null);
        adb.setPositiveButton("Navigate to Miami Beach",
                new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=Miami+Beach,Florida");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        view.startActivity(mapIntent);
                    }
                });
        adb.show();
    }

    /**
     * make request service to show Current Location
     */
    private void showCurrentLocation() {
        // check network status
        if (!CheckNetworkConnection.checkConnection(view)) {
            Toast.makeText(view, "Network not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        // We only start the service to fetch the address if GoogleApiClient is connected.
        if (mGoogleApiClient.isConnected() && mLastLocation != null) {
            startIntentService();
        }
        // If GoogleApiClient isn't connected, we process the user's request by setting
        // mAddressRequested to true. Later, when GoogleApiClient connects, we launch the service to
        // fetch the address. As far as the user is concerned, pressing the Fetch Address button
        // immediately kicks off the process of getting the address.
        mAddressRequested = true;
    }

    /**
     * sent Analytic data
     * @param menuItem
     */
    private void sentAnalytics(MenuItem menuItem) {
        String title = menuItem.getTitle().toString();
        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(view).get(
                AnalyticsTrackers.Target.APP);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Main Activity")
                .setAction("Navigation Drawer Setting Clicked")
                .setLabel(title)
                .build());
    }

    /**
     * show Dialog of error
     */
    public void forceCrash() {
        try {
            throw new RuntimeException("This is a crash");
        } catch (RuntimeException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(view);
            builder.setTitle("Crash Detail");
            builder.setMessage(e.getMessage());
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }
    }
}
