package work.evilz.londonbusstop.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import work.evilz.londonbusstop.R;

/**
 * Fragment of 3 tab fragments about author
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class Fragment_About_Author extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentTabHost mTabHost = new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_about_author);

        // add 3 fragment tabs
        mTabHost.addTab(mTabHost.newTabSpec("Tab1").setIndicator(getResources().getString(R.string.author_tab)),
                Fragment_Author.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("Tab2").setIndicator(getResources().getString(R.string.education_tab)),
                Fragment_Education.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("Tab3").setIndicator(getResources().getString(R.string.contact_tab)),
                Fragment_Contact_Info.class, null);
        /*
        // setting tab properties
        for(int i=0;i<mTabHost.getTabWidget().getChildCount();i++)
        {
            mTabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.color.blue);
        }
        mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).setBackgroundResource(R.color.gray);*/
        return mTabHost;
    }
}
