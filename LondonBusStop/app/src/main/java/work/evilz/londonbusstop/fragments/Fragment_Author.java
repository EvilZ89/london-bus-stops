package work.evilz.londonbusstop.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.analytics.AnalyticsTrackers;
import work.evilz.londonbusstop.constants.Constants;

/**
 * Fragment of author info
 * @author ZeNan Gao
 * @version 09/04/2015
 * @since 1.3
 */
public class Fragment_Author extends Fragment {
    private RelativeLayout mRelativeLayout;
    private EditText mETAge;
    private TextView mTVAge;
    private Button mBTNGuess;
    private static final String mMsgBody1 = "Guess a Bigger Number";
    private static final String mMsgBody2 = "Guess a Smaller Number";
    private static final String mMsgBody3 = "Yeah, You Got it!";
    private static final String mMsgBody4 = "Please enter a number to guess";
    private static final String mMsgBody5 = "Don't be Ridiculous, Guess a Reasonable Age";
    private static final int mAge = 25;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_author, container, false);
        ImageView mIVAuthor = (ImageView) rtView.findViewById(R.id.fragment_author_iv_author);
        mRelativeLayout = (RelativeLayout) rtView.findViewById(R.id.fragment_author_rl_author);
        mTVAge = (TextView) rtView.findViewById(R.id.fragment_author_tv_age);
        mETAge = (EditText) rtView.findViewById(R.id.fragment_author_et_age);
        mBTNGuess = (Button) rtView.findViewById(R.id.fragment_author_btn_guess);

        // hide keyboard
        mETAge.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN){
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        // hide keyboard
                        in.hideSoftInputFromWindow(getView().getApplicationWindowToken(), 0);
                        validateAge();
                    }
                }
                return false;
            }
        });

        mBTNGuess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAge();
            }
        });
        /*
        // create a strong referenced target obj used by picasso
        // and load background image
        Target target = new Target() {
            @Override
            @TargetApi(16)
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    mRelativeLayout.setBackground(new BitmapDrawable(getResources(), bitmap));
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                // do nothing on load fail
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                // do nothing on prepare load
                Log.i("load", "loading background image");
            }
        };
        mRelativeLayout.setTag(target);     // set strong reference
        Picasso.with(getActivity())
                .load(Uri.parse(Constants.mNetworkImageUrl1))
                .into(target);
        */
        // use picasso to display network image in image view
        Picasso.with(getActivity())
                .load(Uri.parse(Constants.mAuthorImageUrl))
                .error(R.mipmap.ic_developer)
                .resize(500, 500)
                .centerCrop()
                .into(mIVAuthor);

        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(getActivity()).get(
                AnalyticsTrackers.Target.APP);
        // Set screen name.
        t.setScreenName(getClass().getSimpleName());
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        return rtView;
    }

    /**
     * validate input on edit text
     */
    private void validateAge() {
        String string = mETAge.getText().toString();
        try {
            if(string.equals(null) && string.equals("")){
                showMessage(mMsgBody4);
            }else {
                int age = Integer.parseInt(string);
                if (age > 100) {
                    showMessage(mMsgBody5);
                } else if (age < mAge) {
                    showMessage(mMsgBody1);
                }else if (age > mAge) {
                    showMessage(mMsgBody2);
                }else if (age == mAge) {
                    showMessage(mMsgBody3);
                    mTVAge.setText("Age: " + mAge);
                }
            }
        } catch (Exception e) {
            showMessage(e.toString());
        }
    }

    /**
     * show Message with certain message
     * @param body dialog body to be displayed
     */
    private void showMessage(String body) {
        Toast.makeText(getActivity(), body, Toast.LENGTH_SHORT).show();
    }
}
