package work.evilz.londonbusstop.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.analytics.AnalyticsTrackers;

/**
 * Fragment of author contact methods
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class Fragment_Contact_Info extends Fragment {
    private TextView mTVPhone;
    private TextView mTVEmail;
    private TextView mTVWebsite;
    private RelativeLayout mRelativeLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_contact, container, false);
        mTVPhone = (TextView) rtView.findViewById(R.id.fragment_contact_tv_phone);
        mTVEmail = (TextView) rtView.findViewById(R.id.fragment_contact_tv_email);
        mTVWebsite = (TextView) rtView.findViewById(R.id.fragment_contact_tv_website);
        mRelativeLayout = (RelativeLayout) rtView.findViewById(R.id.fragment_contact_rl_contact);

        // set hyperlink movement
        mTVPhone.setMovementMethod(LinkMovementMethod.getInstance());
        mTVEmail.setMovementMethod(LinkMovementMethod.getInstance());
        mTVWebsite.setMovementMethod(LinkMovementMethod.getInstance());

        /*
        // create a strong referenced target obj used by picasso
        // and load background image
        Target target = new Target() {
            @Override
            @TargetApi(16)
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    mRelativeLayout.setBackground(new BitmapDrawable(getResources(), bitmap));
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                // do nothing on load fail
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                // do nothing on prepare load
                Log.i("load", "loading background image");
            }
        };
        mRelativeLayout.setTag(target);     // set strong reference
        Picasso.with(getActivity())
                .load(Uri.parse(Constants.mNetworkImageUrl2))
                .into(target);
        */

        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(getActivity()).get(
                AnalyticsTrackers.Target.APP);
        // Set screen name.
        t.setScreenName(getClass().getSimpleName());
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        return rtView;
    }
}
