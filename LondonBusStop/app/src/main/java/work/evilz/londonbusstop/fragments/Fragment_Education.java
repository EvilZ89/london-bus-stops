package work.evilz.londonbusstop.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.analytics.AnalyticsTrackers;
import work.evilz.londonbusstop.constants.Constants;

/**
 * Fragment about author education
 * @author ZeNan Gao
 * @version 09/04/2015
 * @since 1.2
 */
public class Fragment_Education extends Fragment implements TextToSpeech.OnInitListener{
    @Bind(R.id.fragment_education_iv_university)
    ImageView mIVUniversity;
    @Bind(R.id.fragment_education_rl_education)
    RelativeLayout mRelativeLayout;
    @Bind(R.id.fragment_education_tv_motto)
    TextView mTVMotto;
    private TextToSpeech textToSpeech;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_education, container, false);
        // initialize Butter knife
        ButterKnife.bind(this, rtView);

        // use picasso to load university photo
        Picasso.with(getActivity())
                .load(Uri.parse(Constants.mUniversityUrl))
                .error(R.mipmap.ic_developer)
                .resize(500, 500)
                .centerCrop()
                .into(mIVUniversity);
        /*
        // create a strong referenced target obj used by picasso
        // and load background image
        Target target = new Target() {
            @Override
            @TargetApi(16)
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    mRelativeLayout.setBackground(new BitmapDrawable(getResources(), bitmap));
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                // do nothing on load fail
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                // do nothing on prepare load
                Log.i("load", "loading background image");
            }
        };
        mRelativeLayout.setTag(target);     // set strong reference
        Picasso.with(getActivity())
                .load(Uri.parse(Constants.mNetworkImageUrl3))
                .into(target);
        */

        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(getActivity()).get(
                AnalyticsTrackers.Target.APP);
        // Set screen name.
        t.setScreenName(getClass().getSimpleName());
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        return rtView;
    }

    /**
     * listener for TTS init
     * @param status signal the completion of the TextToSpeech engine initialization
     */
    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = textToSpeech.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("Error_TTS", Constants.mErrorTTSLanguageNotSupport);
            }
        } else {
            Log.e("Error_TTS", Constants.mErrorTTSInitializationFail);
        }
    }

    @Override
    public void onStart() {
        // initialize Text to Speech obj
        textToSpeech = new TextToSpeech(getActivity().getApplication(), this);
        super.onStart();
    }

    /**
     * stop text to Speech
     */
    @Override
    public void onStop() {
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        super.onStop();
    }

    /**
     * close text to speech obj
     */
    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            Log.i("TextToSpeech", "closing TTS");
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    /**
     * set listener for motto text view click event using butter knife
     */
    @OnClick(R.id.fragment_education_tv_motto)
    public void speakText() {
        String text = mTVMotto.getText().toString();
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }
}