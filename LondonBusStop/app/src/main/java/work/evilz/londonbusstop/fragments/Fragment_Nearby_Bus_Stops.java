package work.evilz.londonbusstop.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import work.evilz.londonbusstop.analytics.AnalyticsTrackers;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.adapters.BusStopAdapter;
import work.evilz.londonbusstop.helpers.BusStopInfo;

import static work.evilz.londonbusstop.constants.Constants.*;
/**
 * Fragment showing nearby stations and markers of station on google map fragment
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class Fragment_Nearby_Bus_Stops extends Fragment
        implements OnMapReadyCallback{

    //define variables
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<BusStopInfo> busStopList = new ArrayList<>();
    private GoogleMap mMap;

    /**
     * inflate the view and sync google map
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_nearby_bus_stop,container,false);

        // initialize google map fragment
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //STEP 1: Initialize the RecyclerView
        recyclerView = (RecyclerView) rtView.findViewById(R.id.fragment_nearby_bus_stop_rv_recyclerView);
        recyclerView.setHasFixedSize(true);

        //STEP 2: Set the Layout
        //layoutManager = new GridLayoutManager(this,2);
        //layoutManager = new GridLayoutManager(getActivity(),2,GridLayoutManager.VERTICAL,false);
        layoutManager = new LinearLayoutManager(getActivity());
        //  layoutManager = new StaggeredGridLayoutManager(2,1);
        recyclerView.setLayoutManager(layoutManager);

        //STEP 3: Set the Animator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        //STEP 3.5: get data from argument bundle
        Bundle bd = getArguments();
        busStopList = bd.getParcelableArrayList(Tag_stop);

        //STEP 4: Set Adapters
        adapter = new BusStopAdapter(getActivity(), busStopList);
        recyclerView.setAdapter(adapter);

        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(getActivity()).get(
                AnalyticsTrackers.Target.APP);
        // Set screen name.
        t.setScreenName(getClass().getSimpleName());
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        return rtView;
    }

    /**
     * store map reference after map fragment initialized
     * @param googleMap  map reference after map fragment initialized
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Hide the zoom controls as the button panel will cover it.
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setTrafficEnabled(true);
        addMarkersToMap();

    }

    /**
     * add markers to map fragment
     * focus camera
     */
    private void addMarkersToMap(){
        // add markers to map fragment using data's lat + long
        for(BusStopInfo x : busStopList) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(x.getLatitude(), x.getLongitude()))
                    .title(x.getName())
                    .snippet(Tag_bearing + mStringSeparator + x.getBearing()));
        }

        // add marker for calling lat + long
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(mFakeLat, mFakeLng))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_pin))
                .title(mCurrentPositionMsg));

        // focus camera
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mFakeLat, mFakeLng), 16));

    }
}
