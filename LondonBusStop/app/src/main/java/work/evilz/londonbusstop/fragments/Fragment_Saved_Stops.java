package work.evilz.londonbusstop.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import work.evilz.londonbusstop.analytics.AnalyticsTrackers;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.activities.Activity_London_Bus_Stops;
import work.evilz.londonbusstop.adapters.SavedStopsAdapter;
import work.evilz.londonbusstop.helpers.BusStopBasicInfo;
import work.evilz.londonbusstop.helpers.BusStopSQLiteHelper;

/**
 * Fragment about showing list of saved bus stops in database
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class Fragment_Saved_Stops extends Fragment {
    private Button mBtnShowNearbyStations;
    private ListView mLVSavedStops;
    private TextView mTVMessage;
    private ArrayList<BusStopBasicInfo> mSavedStopInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_saved_stops, container, false);
        mBtnShowNearbyStations = (Button)rtView.findViewById(R.id.fragment_saved_stops_btn_show_stops);
        mBtnShowNearbyStations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity_London_Bus_Stops) getActivity()).controller.showNearbyStop();
            }
        });
        mLVSavedStops = (ListView) rtView.findViewById(R.id.fragment_saved_stops_lv_list);
        mTVMessage = (TextView) rtView.findViewById(R.id.fragment_saved_stops_tv_msg);
        refreshListView();

        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(getActivity()).get(
                AnalyticsTrackers.Target.APP);
        // Set screen name.
        t.setScreenName(getClass().getSimpleName());
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        return rtView;
    }

    /**
     * Load list of saved bus stops from database and display it
     */
    public void refreshListView(){
        BusStopSQLiteHelper dbHandler = new BusStopSQLiteHelper(getActivity());
        mSavedStopInfo = dbHandler.Get_All_Bus_Stop_Info();
        int count = dbHandler.Get_Total_Saved_Stops();
        dbHandler.close();
        if (count == 0) {
            mTVMessage.setText("");
        } else {
            mTVMessage.setText(R.string.saved_bus_stop);
        }
        SavedStopsAdapter adapter = new SavedStopsAdapter(this,
                R.layout.listview_saved_stop, mSavedStopInfo);
        mLVSavedStops.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
