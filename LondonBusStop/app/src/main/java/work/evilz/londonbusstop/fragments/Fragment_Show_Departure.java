package work.evilz.londonbusstop.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

import work.evilz.londonbusstop.analytics.AnalyticsTrackers;
import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.adapters.ExpandableListAdapter;
import work.evilz.londonbusstop.helpers.BusStopBasicInfo;

import static work.evilz.londonbusstop.constants.Constants.Tag_BusStopInfo;
import static work.evilz.londonbusstop.constants.Constants.Tag_ELChildList;
import static work.evilz.londonbusstop.constants.Constants.Tag_ELKey;
import static work.evilz.londonbusstop.constants.Constants.Tag_ELParentSize;
import static work.evilz.londonbusstop.constants.Constants.Tag_bearing;
import static work.evilz.londonbusstop.constants.Constants.mCurrentPositionMsg;
import static work.evilz.londonbusstop.constants.Constants.mFakeLat;
import static work.evilz.londonbusstop.constants.Constants.mFakeLng;
import static work.evilz.londonbusstop.constants.Constants.mStringSeparator;

/**
 * Fragment using expandable list to display bus departure time for every bus line on
 * particular bus stop in coming hour and show bus stop as marker on google map fragment
 * @link http://www.androidhive.info/2013/07/android-expandable-list-view-tutorial/
 * @author ZeNan Gao
 * @version 08/26/2015
 * @since 1.0
 */
public class Fragment_Show_Departure extends Fragment
        implements OnMapReadyCallback{
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private ArrayList<String> listDataHeader;
    private HashMap<String, ArrayList<String>> listDataChild;
    private GoogleMap mMap;
    private BusStopBasicInfo mInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_bus_departure, container, false);
        expListView = (ExpandableListView) rtView.findViewById(R.id.fragment_bus_departure_elv_departure);

        // preparing list data
        Bundle bd = getArguments();
        mInfo = bd.getParcelable(Tag_BusStopInfo);
        prepareListData(bd);
        Log.d("bundle", bd.toString());

        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);
        // ListView Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        // ListView Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                showToastText(listDataHeader.get(groupPosition) + " Expanded");
            }
        });

        // ListView Group collapsed listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                showToastText(listDataHeader.get(groupPosition) + " Collapsed");
            }
        });

        // ListView on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                showToastText(listDataHeader.get(groupPosition) + " : "
                        + listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                return false;
            }
        });

        // initialize google map fragment
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);

        // Get tracker.
        Tracker t = AnalyticsTrackers.getInstance(getActivity()).get(
                AnalyticsTrackers.Target.APP);
        // Set screen name.
        t.setScreenName(getClass().getSimpleName());
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        return rtView;
    }

    /**
     * add markers to map fragment
     * focus camera
     */
    public void addMarker(){
        // add bus stop marker
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(mInfo.getLatitude(), mInfo.getLongitude()))
                .title(mInfo.getName())
                .snippet(Tag_bearing + mStringSeparator + mInfo.getBearing()));

        // add marker for calling lat  + long
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(mFakeLat, mFakeLng))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_pin))
                .title(mCurrentPositionMsg));

        // focus camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mFakeLat, mFakeLng), 16));
    }

    /**
     * store map reference after map fragment initialized
     * @param googleMap  map reference after map fragment initialized
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Hide the zoom controls as the button panel will cover it.
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setTrafficEnabled(true);
        addMarker();
    }

    /**
     * display data on Expandable list
     * @param bd bundle contain data to be displayed on Expandable list
     */
    private void prepareListData(Bundle bd) {
        listDataHeader = bd.getStringArrayList(Tag_ELKey);
        listDataChild = new HashMap<>();

        int size = bd.getInt(Tag_ELParentSize);
        for (int i = 0; i < size; i++) {
            listDataChild.put(listDataHeader.get(i),
                    bd.getStringArrayList(Tag_ELChildList + i));
        }
    }

    /**
     * display Toast msg given message
     * @param msg message used to display Toast msg
     */
    private void showToastText(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }
}
