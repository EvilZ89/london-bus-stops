package work.evilz.londonbusstop.helpers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Parcelable help class for bas stop basic info
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class BusStopBasicInfo implements Parcelable {
    private String atcocode;
    private String name;
    private String bearing;
    private String locality;
    private double latitude;
    private double longitude;

    /**
     * Constructor to create object from parcel
     * @param source source parcel
     */
    public BusStopBasicInfo(Parcel source) {
        atcocode = source.readString();
        name = source.readString();
        bearing = source.readString();
        locality = source.readString();
        latitude = source.readDouble();
        longitude = source.readDouble();
    }

    /**
     * get bus atcocode
     * @return bus stop atcocode
     */
    public String getAtcocode() {
        return this.atcocode;
    }

    /**
     * get bus stop name
     * @return bus stop name
     */
    public String getName() {
        return this.name;
    }

    /**
     * get bus stop bearing
     * @return bus stop bearing
     */
    public String getBearing() {
        return this.bearing;
    }

    /**
     * get bus stop locality
     * @return  bus stop locality
     */
    public String getLocality() {
        return this.locality;
    }

    /**
     * get bus stop Longitude
     * @return bus stop longitude
     */
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * get bus stop Latitude
     * @return bus stop latitude
     */
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * default create Constructor
     * @param atcocode bus stop atcocode
     * @param name bus stop name
     * @param bearing bus stop bearing
     * @param locality bus stop locality
     * @param latitude bus stop latitude
     * @param longitude bus stop longitude
     */
    public BusStopBasicInfo(String atcocode, String name, String bearing,
                            String locality, double latitude, double longitude) {
        this.atcocode = atcocode;
        this.name = name;
        this.bearing = bearing;
        this.locality = locality;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable's marshalled representation
     * @return a bitmask indicating the set of special object types marshalled by the Parcelable
     */
    @Override
    public int describeContents() {
        return this.hashCode();
    }

    /**
     * write to parcel obj
     * @param dest dest parcel
     * @param flags not used
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(atcocode);
        dest.writeString(name);
        dest.writeString(bearing);
        dest.writeString(locality);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    /**
     * re-create obj from parcel
     */
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public BusStopInfo createFromParcel(Parcel in) {
            return new BusStopInfo(in);
        }

        public BusStopInfo[] newArray(int size) {
            return new BusStopInfo[size];
        }
    };
}
