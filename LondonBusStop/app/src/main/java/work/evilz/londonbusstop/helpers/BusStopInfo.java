package work.evilz.londonbusstop.helpers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Parcelable help class for bas stop info
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class BusStopInfo implements Parcelable {
    private String atcocode;
    private String smscode;
    private String name;
    private String bearing;
    private String locality;
    private String indicator;
    private double longitude;
    private double latitude;
    private int distance;

    /**
     * Constructor to create object from parcel
     * @param source source parcel
     */
    public BusStopInfo(Parcel source) {
        atcocode = source.readString();
        smscode = source.readString();
        name = source.readString();
        bearing = source.readString();
        locality = source.readString();
        indicator = source.readString();
        latitude = source.readDouble();
        longitude = source.readDouble();
        distance = source.readInt();
    }

    /**
     * get bus atcocode
     * @return bus stop atcocode
     */
    public String getAtcocode() {
        return this.atcocode;
    }

    /**
     * get bus smscode
     * @return bus stop smscode
     */
    public String getSmscode() {
        return this.smscode;
    }

    /**
     * get bus stop name
     * @return bus stop name
     */
    public String getName() {
        return this.name;
    }

    /**
     * get bus stop bearing
     * @return bus stop bearing
     */
    public String getBearing() {
        return this.bearing;
    }

    /**
     * get bus stop locality
     * @return  bus stop locality
     */
    public String getLocality() {
        return this.locality;
    }

    /**
     * get bus stop indicator
     * @return  bus stop indicator
     */
    public String getIndicator() {
        return this.indicator;
    }

    /**
     * get bus stop Longitude
     * @return bus stop longitude
     */
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * get bus stop Latitude
     * @return bus stop latitude
     */
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * get bus stop distance from you
     * @return bus stop distance from you
     */
    public int getDistance() {
        return this.distance;
    }

    /**
     * default create Constructor
     * @param atcocode bus stop atcocode
     * @param smscode bus stop smscode
     * @param name bus stop name
     * @param bearing bus stop bearing
     * @param locality bus stop locality
     * @param indicator bus stop indicator
     * @param latitude bus stop latitude
     * @param longitude bus stop longitude
     * @param distance bus stop distance from you
     */
    public BusStopInfo(String atcocode, String smscode,
                       String name, String bearing,
                       String locality,String indicator,
                       double latitude,double longitude,
                       int distance) {
        this.atcocode = atcocode;
        this.smscode = smscode;
        this.name = name;
        this.bearing = bearing;
        this.locality = locality;
        this.indicator = indicator;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable's marshalled representation
     * @return a bitmask indicating the set of special object types marshalled by the Parcelable
     */
    @Override
    public int describeContents() {
        return this.hashCode();
    }

    /**
     * write to parcel obj
     * @param dest dest parcel
     * @param flags not used
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(atcocode);
        dest.writeString(smscode);
        dest.writeString(name);
        dest.writeString(bearing);
        dest.writeString(locality);
        dest.writeString(indicator);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(distance);
    }

    /**
     * re-create obj from parcel
     */
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public BusStopInfo createFromParcel(Parcel in) {
            return new BusStopInfo(in);
        }

        public BusStopInfo[] newArray(int size) {
            return new BusStopInfo[size];
        }
    };
}
