package work.evilz.londonbusstop.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * SQLite database helper class
 * @author ZeNan Gao
 * @version 09/04/2015
 * @since 1.2
 */
public class BusStopSQLiteHelper extends SQLiteOpenHelper {
    // All Static variables

    // Database Version
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bus_stops.db";
    private static final String TABLE_BUS_STOPS = "bus_stops";

    // Contacts Table Columns names
    private static final String KEY_ID = "_id";
    private static final String KEY_ATCOCODE = "atcocode";
    private static final String KEY_NAME = "name";
    private static final String KEY_BEARING = "bearing";
    private static final String KEY_LOCALITY = "locality";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    // Database table Creation sql statement
    private static final String CREATE_BUS_STOPS_TABLE = "CREATE TABLE "
            + TABLE_BUS_STOPS + "(" + KEY_ID + " INT PRIMARY KEY,"
            + KEY_ATCOCODE + " TEXT," + KEY_NAME + " TEXT,"
            + KEY_BEARING + " TEXT," + KEY_LOCALITY + " TEXT,"
            + KEY_LATITUDE + " REAL," + KEY_LONGITUDE + " REAL" + ")";

    /**
     * default constructor initialize database
     * @param context context the database will be associated with
     */
    public BusStopSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Creating Tables
     * @param db database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BUS_STOPS_TABLE);
    }

    /**
     * update database
     * @param db database
     * @param oldVersion database old version
     * @param newVersion database new version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(BusStopSQLiteHelper.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion);

        // drop table if exist and create new database
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUS_STOPS);
        onCreate(db);
    }

    // All CRUD(Create, Read, Update, Delete) Operations
    /**
     * inset into database
     * @param info bus stop info obj contain info to be inserted into sql database
     */
    public void Add_BusStop(BusStopInfo info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ATCOCODE, info.getAtcocode());
        values.put(KEY_NAME, info.getName());
        values.put(KEY_BEARING, info.getBearing());
        values.put(KEY_LOCALITY, info.getLocality());
        values.put(KEY_LATITUDE, info.getLatitude());
        values.put(KEY_LONGITUDE, info.getLongitude());

        // Inserting Row
        db.insert(TABLE_BUS_STOPS, null, values);
        db.close();
    }

    /**
     * Getting single stop using atcocode
     * @param atcocode bus stop atcocode
     */
    public BusStopBasicInfo Get_Single_Stop(String atcocode) {
        SQLiteDatabase db = this.getReadableDatabase();
        BusStopBasicInfo info = null;
        Cursor cursor = db.query(TABLE_BUS_STOPS, null, KEY_ATCOCODE + "=?",
                new String[]{atcocode}, null, null, null, null);
        // Log.d("cursor", cursor.getCount()+"");
        if (cursor.getCount() !=0) {
            cursor.moveToFirst();
            try {
                info = new BusStopBasicInfo(
                        cursor.getString(1), cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), Double.parseDouble(cursor.getString(5)),
                        Double.parseDouble(cursor.getString(6)));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        cursor.close();
        db.close();
        return info;
    }

    /**
     * getting all bus stop info in database
     */
    public ArrayList<BusStopBasicInfo> Get_All_Bus_Stop_Info() {
        ArrayList<BusStopBasicInfo> busStopInfo = new ArrayList<>();
        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_BUS_STOPS;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    BusStopBasicInfo info = new BusStopBasicInfo(
                            cursor.getString(1), cursor.getString(2),cursor.getString(3),
                            cursor.getString(4), Double.parseDouble(cursor.getString(5)),
                            Double.parseDouble(cursor.getString(6)));
                    // Adding contact to list
                    busStopInfo.add(info);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return busStopInfo;
    }

    /**
     * delete record from database
     * @param atcocode bus stop atcocode
     */
    public void Delete_Stop(String atcocode) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BUS_STOPS, KEY_ATCOCODE + " = ?", new String[]{atcocode});
        db.close();
    }

    /**
     * get number of saved bus stops
     * @return number of saved bus stops info record in database
     */
    public int Get_Total_Saved_Stops() {
        String countQuery = "SELECT  * FROM " + TABLE_BUS_STOPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        // return count
        return count;
    }
}
