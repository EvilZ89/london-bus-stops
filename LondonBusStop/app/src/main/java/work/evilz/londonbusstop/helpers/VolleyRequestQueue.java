package work.evilz.londonbusstop.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * volley request queue singleton class
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class VolleyRequestQueue {

    private RequestQueue mRequestQueue;
    private static VolleyRequestQueue mInstance;
    private ImageLoader mImageLoader;
    private static Context mContext;
    private static String TAG = VolleyRequestQueue.class.getSimpleName();

    /**
     * associated volley request queue with invoking context
     * @param context context which the request queue will be associated with
     */
    private VolleyRequestQueue(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    /**
     * create singleton obj if not exist and return that obj
     * @param context context which the request queue will be associated with
     * @return single unique instance/object associated with context
     */
    public static synchronized VolleyRequestQueue getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new VolleyRequestQueue(context);
        }
        return mInstance;
    }

    /**
     * get volley request queue
     * @return volley request queue
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * add volley request to request queue
     * @param req volley request
     * @param tag tag for request
     * @param <T> volley request type
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    /**
     * add volley request to request queue
     * @param req volley request
     * @param <T> volley request type
     */
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    /**
     * get image loader
     * @return image loader
     */
    public ImageLoader getImageLoader() {
        return mImageLoader;
    }
}
