package work.evilz.londonbusstop.models;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import work.evilz.londonbusstop.R;
import work.evilz.londonbusstop.controllers.Controller_Activity_London_Bus_Stops;
import work.evilz.londonbusstop.fragments.Fragment_About_Author;
import work.evilz.londonbusstop.fragments.Fragment_Nearby_Bus_Stops;
import work.evilz.londonbusstop.fragments.Fragment_Saved_Stops;
import work.evilz.londonbusstop.fragments.Fragment_Show_Departure;
import work.evilz.londonbusstop.helpers.BusStopBasicInfo;
import work.evilz.londonbusstop.helpers.BusStopInfo;
import work.evilz.londonbusstop.helpers.VolleyRequestQueue;
import work.evilz.londonbusstop.system_utils.CheckNetworkConnection;

import static work.evilz.londonbusstop.constants.Constants.Tag_BusStopInfo;
import static work.evilz.londonbusstop.constants.Constants.Tag_ELChildList;
import static work.evilz.londonbusstop.constants.Constants.Tag_ELKey;
import static work.evilz.londonbusstop.constants.Constants.Tag_ELParentSize;
import static work.evilz.londonbusstop.constants.Constants.Tag_SharedPreference;
import static work.evilz.londonbusstop.constants.Constants.Tag_atcocode;
import static work.evilz.londonbusstop.constants.Constants.Tag_bearing;
import static work.evilz.londonbusstop.constants.Constants.Tag_departure;
import static work.evilz.londonbusstop.constants.Constants.Tag_departure_time;
import static work.evilz.londonbusstop.constants.Constants.Tag_distance;
import static work.evilz.londonbusstop.constants.Constants.Tag_indicator;
import static work.evilz.londonbusstop.constants.Constants.Tag_latitude;
import static work.evilz.londonbusstop.constants.Constants.Tag_locality;
import static work.evilz.londonbusstop.constants.Constants.Tag_longitude;
import static work.evilz.londonbusstop.constants.Constants.Tag_name;
import static work.evilz.londonbusstop.constants.Constants.Tag_rate_count;
import static work.evilz.londonbusstop.constants.Constants.Tag_smscode;
import static work.evilz.londonbusstop.constants.Constants.Tag_stop;
import static work.evilz.londonbusstop.constants.Constants.mAuthToken;
import static work.evilz.londonbusstop.constants.Constants.mConfirm;
import static work.evilz.londonbusstop.constants.Constants.mCurrentDateTime;
import static work.evilz.londonbusstop.constants.Constants.mCurrentOSName;
import static work.evilz.londonbusstop.constants.Constants.mCurrentOSVersion;
import static work.evilz.londonbusstop.constants.Constants.mDepartureAPIFooter;
import static work.evilz.londonbusstop.constants.Constants.mDepartureAPIHeader;
import static work.evilz.londonbusstop.constants.Constants.mFakeLat;
import static work.evilz.londonbusstop.constants.Constants.mFakeLng;
import static work.evilz.londonbusstop.constants.Constants.mLatArg;
import static work.evilz.londonbusstop.constants.Constants.mLngArg;
import static work.evilz.londonbusstop.constants.Constants.mNearbyStopsAPI;
import static work.evilz.londonbusstop.constants.Constants.mStringSeparator;

/**
 * model of MVC pattern
 * @author ZeNan Gao
 * @version 09/06/2015
 * @since 1.5
 */
public class Model_Activity_London_Bus_Stops {
    private Controller_Activity_London_Bus_Stops mController;
    private ProgressDialog mDialog;
    private SharedPreferences mSharedPreference;

    /**
     * default constructor
     * @param mController ref to controller
     */
    public Model_Activity_London_Bus_Stops(Controller_Activity_London_Bus_Stops mController) {
        this.mController = mController;
        mSharedPreference = mController.view.getSharedPreferences(Tag_SharedPreference, 0);
    }

    /**
     * use volley to get nearby bus stop info
     */
    public void showNearbyStops(){
        // check network status
        if (!CheckNetworkConnection.checkConnection(mController.view)) {
            Toast.makeText(mController.view, "Network not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                mNearbyStopsAPI + mLatArg + mFakeLat + mLngArg + mFakeLng + mAuthToken,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", mNearbyStopsAPI + mLatArg + mFakeLat + mLngArg + mFakeLng + mAuthToken);
                        ArrayList<BusStopInfo> BusStopList = new ArrayList<>();
                        try {
                            JSONArray busStops = response.getJSONArray(Tag_stop);
                            JSONObject c;
                            for (int i = 0; i < busStops.length(); i++) {
                                c = busStops.getJSONObject(i);
                                BusStopList.add(new BusStopInfo(
                                        c.getString(Tag_atcocode),
                                        c.getString(Tag_smscode),
                                        c.getString(Tag_name),
                                        c.getString(Tag_bearing),
                                        c.getString(Tag_locality),
                                        c.getString(Tag_indicator),
                                        c.getDouble(Tag_latitude),
                                        c.getDouble(Tag_longitude),
                                        c.getInt(Tag_distance)
                                ));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showFragmentNearbyStops(BusStopList);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });
        // Adding request to request queue
        VolleyRequestQueue.getInstance(mController.view).addToRequestQueue(req);
    }

    /**
     * use volley to get bus departure info for particular stop in coming hour
     * @param info bus stop info
     */
    private void getStopDeparture(final BusStopBasicInfo info) {
        // check network status
        if (!CheckNetworkConnection.checkConnection(mController.view)) {
            Toast.makeText(mController.view, "Network not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                mDepartureAPIHeader + info.getAtcocode() + mDepartureAPIFooter + mAuthToken,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", mDepartureAPIHeader + info.getAtcocode() + mDepartureAPIFooter + mAuthToken);
                        //Log.d("response", response.toString());
                        Bundle bd = new Bundle();
                        bd.putParcelable(Tag_BusStopInfo, info);
                        try {
                            JSONObject c = response.getJSONObject(Tag_departure);
                            int size = c.length();
                            bd.putInt(Tag_ELParentSize, size);
                            ArrayList<String> keys = new ArrayList<>();
                            Iterator<String> iterator = c.keys();
                            int parent_count = 0;
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                keys.add(key);
                                JSONArray d = c.getJSONArray(key);
                                ArrayList<String> time = new ArrayList<>();
                                for (int i = 0; i < d.length(); i++) {
                                    time.add(d.getJSONObject(i).getString(Tag_departure_time));
                                }
                                bd.putStringArrayList(Tag_ELChildList + parent_count++, time);
                            }
                            bd.putStringArrayList(Tag_ELKey, keys);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showStopDepartureTime(bd);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });
        // Adding request to request queue
        VolleyRequestQueue.getInstance(mController.view).addToRequestQueue(req);
    }

    /**
     * show progress dialog while loading
     */
    private void showProgressDialog(){
        mDialog = new ProgressDialog(mController.view);
        mDialog.setMessage("Loading, Please Wait");
        mDialog.setCancelable(false);
        mDialog.show();
    }

    /**
     * hide progress dialog when loading finish
     */
    private void hideProgressDialog(){
        mDialog.dismiss();
    }

    /**
     * show bus departure using bus stop info
     * @param info bus stop info used for departure
     */
    public void showStopDeparture(BusStopBasicInfo info) {
        //Toast.makeText(mController.view, "Code: " + info.getAtcocode(), Toast.LENGTH_LONG).show();
        getStopDeparture(info);

    }

    /**
     * show Fragment_Nearby_Bus_Stops in fragment container
     * @param list array list of parcelable object to be passed into fragment
     */
    private void showFragmentNearbyStops(ArrayList<BusStopInfo> list){
        Fragment fragment = new Fragment_Nearby_Bus_Stops();
        Bundle bd = new Bundle();
        bd.putParcelableArrayList(Tag_stop, list);
        fragment.setArguments(bd);
        FragmentTransaction fragmentTransaction = mController.view.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_london_bus_stops_fragment_container, fragment, "Fragment_Nearby_Stop");
        fragmentTransaction.commit();
    }

    /**
     * show Fragment_Show_Departure in fragment container
     * @param bundle bundle to be passed into fragment
     */
    private void showStopDepartureTime(Bundle bundle){
        Fragment fragment = new Fragment_Show_Departure();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = mController.view.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_london_bus_stops_fragment_container, fragment, "Fragment_Show_Stop_Departure");
        fragmentTransaction.commit();
    }

    /**
     * show Fragment_About_Author in fragment container
     */
    public void showAuthorInfo(){
        Fragment fragment = new Fragment_About_Author();
        FragmentTransaction fragmentTransaction = mController.view.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_london_bus_stops_fragment_container, fragment, "Fragment_About_Author");
        fragmentTransaction.commit();
    }

    /**
     * show Fragment_Saved_Stops in fragment container
     */
    public void showSavedStops(){
        Fragment fragment = new Fragment_Saved_Stops();
        FragmentTransaction fragmentTransaction = mController.view.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_london_bus_stops_fragment_container, fragment, "Fragment_Show_Saved_Stop");
        fragmentTransaction.commit();
    }

    /**
     * use alert dialog box to display system variables
     */
    public void showCurrentSystemVariables() {
        String osVersion = System.getProperty("os.version");
        String osName = System.getProperty("os.name");
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss", Locale.US);
        String strDateTime = sdf.format(calendar.getTime());
        int count = mSharedPreference.getInt(Tag_rate_count, 0);

        // add to result list
        ArrayList<String> result = new ArrayList<>();
        result.add(mCurrentOSName + osName);
        result.add(mCurrentOSVersion + osVersion);
        result.add(mCurrentDateTime + strDateTime);
        result.add(Tag_rate_count + mStringSeparator + count);
        String stringToDisplay = TextUtils.join(System.getProperty("line.separator"), result);

        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(mController.view);
        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(stringToDisplay)
                .setTitle(R.string.system_variables)
                .setPositiveButton(mConfirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do Nothing
                    }
                });
        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * store count of rate click in Shared preference
     */
    public void rateApp() {
        int count = mSharedPreference.getInt(Tag_rate_count, 0);
        count++;
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putInt(Tag_rate_count, count);
        editor.apply();
    }
}
