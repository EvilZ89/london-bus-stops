package work.evilz.londonbusstop.services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static work.evilz.londonbusstop.constants.Constants.ERROR_INTENT_SERVICE;
import static work.evilz.londonbusstop.constants.Constants.FAILURE_RESULT;
import static work.evilz.londonbusstop.constants.Constants.LOCATION_DATA_EXTRA;
import static work.evilz.londonbusstop.constants.Constants.MESSAGE_SUCCESS;
import static work.evilz.londonbusstop.constants.Constants.RECEIVER;
import static work.evilz.londonbusstop.constants.Constants.RESPONSE_DATA_ERROR;
import static work.evilz.londonbusstop.constants.Constants.RESPONSE_LAT_LNG_ERROR;
import static work.evilz.londonbusstop.constants.Constants.RESPONSE_NULL_ADDRESS;
import static work.evilz.londonbusstop.constants.Constants.RESPONSE_RECEIVER_ERROR;
import static work.evilz.londonbusstop.constants.Constants.RESPONSE_SERVICE_ERROR;
import static work.evilz.londonbusstop.constants.Constants.RESULT_DATA_KEY;
import static work.evilz.londonbusstop.constants.Constants.SUCCESS_RESULT;
import static work.evilz.londonbusstop.constants.Constants.TAG_FETCH_ADDRESS_INTENT_SERVICE;

/**
 * Class for fetching location using intent service
 * Asynchronously handles an intent using a worker thread. Receives a ResultReceiver object and a
 * location through an intent. Tries to fetch the address for the location using a Geocoder, and
 * sends the result to the ResultReceiver.
 * Modified from Google Fetch Address example
 * @author ZeNan Gao
 * @version 08/30/2015
 * @since 1.1
 */
public class FetchAddressIntentService extends IntentService {
    /**
     * The receiver where results are forwarded from this service.
     */
    protected ResultReceiver mReceiver;

    /**
     * This constructor is required, and calls the super IntentService(String)
     * constructor with the name for a worker thread.
     */
    public FetchAddressIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG_FETCH_ADDRESS_INTENT_SERVICE);
    }

    /**
     * Tries to get the location address using a Geocoder. If successful, sends an address to a
     * result receiver. If unsuccessful, sends an error message instead.
     * Note: We define a {@link android.os.ResultReceiver} in * MainActivity to process content
     * sent from -this service.
     *
     * This service calls this method from the default worker thread with the intent that started
     * the service. When this method returns, the service automatically stops.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        mReceiver = intent.getParcelableExtra(RECEIVER);

        // Check if receiver was properly registered.
        if (mReceiver == null) {
            Log.wtf(TAG_FETCH_ADDRESS_INTENT_SERVICE, RESPONSE_RECEIVER_ERROR);
            return;
        }

        // Get the location passed to this service through an extra.
        Location location = intent.getParcelableExtra(LOCATION_DATA_EXTRA);

        // Make sure that the location data was really sent over through an extra. If it wasn't,
        // send an error error message and return.
        if (location == null) {
            Log.wtf(TAG_FETCH_ADDRESS_INTENT_SERVICE, RESPONSE_DATA_ERROR);
            deliverResultToReceiver(FAILURE_RESULT, RESPONSE_DATA_ERROR);
            return;
        }

        // Errors could still arise from using the Geocoder (for example, if there is no
        // connectivity, or if the Geocoder is given illegal location data). Or, the Geocoder may
        // simply not have an address for a location. In all these cases, we communicate with the
        // receiver using a resultCode indicating failure. If an address is found, we use a
        // resultCode indicating success.

        // The Geocoder used in this sample. The Geocoder responses are localized for the given
        // Locale, which represents a specific geographical or linguistic region. Locales are used
        // to alter the presentation of information such as numbers or dates to suit the conventions
        // in the region they describe.
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        // Address found using the Geocoder.
        List<Address> addresses = null;

        try {
            // Using getFromLocation() returns an array of Addresses for the area immediately
            // surrounding the given latitude and longitude. The results are a best guess and are
            // not guaranteed to be accurate.
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException e) {
            // Catch network or other I/O problems.
            Log.e(TAG_FETCH_ADDRESS_INTENT_SERVICE, RESPONSE_SERVICE_ERROR, e);
        } catch (IllegalArgumentException e) {
            // Catch invalid latitude or longitude values.
            Log.e(TAG_FETCH_ADDRESS_INTENT_SERVICE, RESPONSE_LAT_LNG_ERROR +
                    " Latitude = " + location.getLatitude() +
                    ", Longitude = " + location.getLongitude(), e);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            Log.e(TAG_FETCH_ADDRESS_INTENT_SERVICE, RESPONSE_NULL_ADDRESS);
            deliverResultToReceiver(FAILURE_RESULT, ERROR_INTENT_SERVICE);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> resultList = new ArrayList<>();

            // Fetch the address lines using {@code getAddressLine},
            // join them, and send them to the thread. The {@link android.location.address}
            // class provides other options for fetching address details that you may prefer to use
            /*
            resultList.add(address.getLocality());          // street name
            resultList.add(address.getAdminArea());         // state abbreviation
            resultList.add(address.getPostalCode());        // zip code
            resultList.add(address.getCountryCode());       // country code (ie. US)
            resultList.add(address.getCountryName());       // country name
            resultList.add(address.getPhone());             // phone number
            resultList.add(address.getPremises());          // premise name
            resultList.add(address.describeContents() + "");// no idea
            resultList.add(address.getAddressLine(0));      // first line of the address
            resultList.add(address.getAddressLine(1));      // city, state, zip code
            resultList.add(address.getAddressLine(2));      // country
            resultList.add(address.getAddressLine(3));      // null
            resultList.add(address.getSubAdminArea());      // null
            resultList.add(address.getSubLocality());       // null
            resultList.add(address.getUrl());               // website url
            resultList.add(address.getThoroughfare());      // street name
            resultList.add(address.getSubThoroughfare());   // street number
            */

            resultList.add("Current Latitude: " + location.getLatitude());
            resultList.add("Current Longitude: " + location.getLongitude());
            resultList.add(address.getAddressLine(0));      // first line of the address
            resultList.add(address.getAddressLine(1));      // city, state, zip code
            resultList.add(address.getAddressLine(2));      // country

            Log.i(TAG_FETCH_ADDRESS_INTENT_SERVICE, MESSAGE_SUCCESS);
            deliverResultToReceiver(SUCCESS_RESULT,
                    TextUtils.join(System.getProperty("line.separator"), resultList));
        }
    }

    /**
     * Sends a resultCode and message to the receiver.
     */
    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(RESULT_DATA_KEY, message);
        mReceiver.send(resultCode, bundle);
    }
}
