package work.evilz.londonbusstop.system_utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * system utility class checking network connection
 * @author ZeNan Gao
 * @version 09/04/2015
 * @since 1.0
 */
public class CheckNetworkConnection {
    /**
     * checking network connection before making network request
     * @param context the invoking context for checking network connection
     * @return true if network ok, false otherwise
     */
    public static boolean checkConnection(Context context){
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }else {
            return false;
        }
    }
}
